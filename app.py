from flask import Flask, request, jsonify
from flask_cors import CORS
import spacy
from collections import Counter
import random

app = Flask(__name__)
CORS(app)

# Load English tokenizer, tagger, parser, NER, and word vectors
nlp = spacy.load("en_core_web_sm")

def generate_mcqs(text, num_questions=5):
    if text is None:
        return []

    # Process the text with spaCy
    doc = nlp(text)

    # Extract sentences from the text
    sentences = [sent.text for sent in doc.sents]

    # Ensure that the number of questions does not exceed the number of sentences
    num_questions = min(num_questions, len(sentences))

    # Randomly select sentences to form questions
    selected_sentences = random.sample(sentences, num_questions)

    # Initialize list to store generated MCQs
    mcqs = []

    # Generate MCQs for each selected sentence
    for sentence in selected_sentences:
        # Process the sentence with spaCy
        sent_doc = nlp(sentence)

        # Extract entities (nouns) from the sentence
        nouns = [token.text for token in sent_doc if token.pos_ == "NOUN"]

        # Ensure there are enough nouns to generate MCQs
        if len(nouns) < 2:
            continue

        # Count the occurrence of each noun
        noun_counts = Counter(nouns)

        # Select the most common noun as the subject of the question
        if noun_counts:
            subject = noun_counts.most_common(1)[0][0]

            # Generate the question stem
            question_stem = sentence.replace(subject, "______")

            # Generate answer choices
            answer_choices = [subject]

            # Add some random words from the text as distractors
            distractors = list(set(nouns) - {subject})

            # Ensure there are at least three distractors
            while len(distractors) < 3:
                distractors.append("None of the above")  # Placeholder for missing distractors

            random.shuffle(distractors)
            for distractor in distractors[:3]:
                answer_choices.append(distractor)

            # Shuffle the answer choices
            random.shuffle(answer_choices)

            # Append the generated MCQ to the list
            correct_answer_index = answer_choices.index(subject)  # Get index of the correct answer
            mcqs.append((question_stem, answer_choices, correct_answer_index))

    return mcqs

@app.route('/api/generate_mcqs', methods=['POST'])
def generate_mcqs_api():
    data = request.get_json()
    text = data.get('text', '')
    num_questions = data.get('num_questions', 5)

    mcqs = generate_mcqs(text, num_questions=num_questions)
    # Convert MCQs to a serializable format
    mcqs_serializable = [
        {
            'question_stem': mcq[0],
            'answer_choices': mcq[1],
            'correct_answer': mcq[2]
        }
        for mcq in mcqs
    ]

    return jsonify(mcqs=mcqs_serializable)

if __name__ == '__main__':
    app.run(debug=True)

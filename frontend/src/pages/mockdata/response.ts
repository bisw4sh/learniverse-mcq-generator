export const response = {
  mcqs: [
    {
      answer_choices: ["switches", "software", "components", "cards"],
      correct_answer: "C",
      question_stem:
        "The fundamental ______ of computer networks include hardware devices like routers, switches, and network interface cards (NICs), as well as software protocols that enable communication, such as TCP/IP.",
    },
    {
      answer_choices: ["systems", "networks", "Computer", "resources"],
      correct_answer: "C",
      question_stem:
        "______ networks are systems that connect multiple computers to share resources and information.",
    },
    {
      answer_choices: ["networks", "area", "countries", "WANs"],
      correct_answer: "A",
      question_stem:
        "These ______ can vary in size and complexity, ranging from small local area ______ (LANs) within a single building to large wide area ______ (WANs) that span cities, countries, or even continents.",
    },
    {
      answer_choices: ["optic", "networks", "development", "accessibility"],
      correct_answer: "B",
      question_stem:
        "The evolution of networking technologies, such as the transition from wired to wireless ______ and the development of high-speed fiber optic connections, has significantly enhanced the performance and accessibility of ______.",
    },
    {
      answer_choices: ["systems", "encryption", "Security", "networks"],
      correct_answer: "C",
      question_stem:
        "______ is a crucial aspect of computer networks, involving measures like firewalls, encryption, and intrusion detection systems (IDS) to protect against unauthorized access and data breaches.",
    },
  ],
};

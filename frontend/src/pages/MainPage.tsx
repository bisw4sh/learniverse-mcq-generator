import {
  Form,
  useLoaderData,
  useActionData,
  ActionFunctionArgs,
} from "react-router-dom";
import { twMerge } from "tailwind-merge";
import clsx from "clsx";
import { request_data } from "./mockdata/request";

interface FormEntries {
  mcqs: string;
  [key: string]: FormDataEntryValue;
}

interface MCQ {
  question: string;
  options: string[];
  correct_answer: number;
  correct_option: string;
}

// eslint-disable-next-line react-refresh/only-export-components
export const loader = async (): Promise<MCQ[]> => {
  const request__data = {
    text: request_data.text, // replace with actual text input
    num_questions: 5,
  };

  const response = await fetch("http://127.0.0.1:5000/api/generate_mcqs", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(request__data),
  });

  const data = await response.json();

  // Transform the received data to match the MCQ interface
  const mcqs: MCQ[] = data.mcqs.map(
    (item: {
      answer_choices: string[];
      correct_answer: number;
      question_stem: string;
    }) => ({
      question: item.question_stem,
      options: item.answer_choices,
      correct_answer: item.correct_answer,
      correct_option: item.answer_choices[item.correct_answer],
    }),
  );

  return mcqs;
};

// eslint-disable-next-line react-refresh/only-export-components
export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const formEntries = Object.fromEntries(formData.entries()) as FormEntries;
  const mcqs: MCQ[] = JSON.parse(formEntries.mcqs);
  const answers: { [key: string]: string } = {};
  for (const [key, value] of Object.entries(formEntries)) {
    if (key !== "mcqs") {
      answers[key] = value as string;
    }
  }

  let correct_answer_num = 0;
  const individual_correct = [];
  for (let i = 0; i < mcqs.length; i++) {
    if (parseInt(answers[`mcq${i}`]) === mcqs[i].correct_answer) {
      individual_correct.push(true);
      correct_answer_num++;
    } else {
      individual_correct.push(false);
    }
  }

  return { correct_answer_num, individual_correct };
};

export default function MainPage() {
  const data = useLoaderData() as MCQ[];
  console.log(data);
  data.forEach((single) => console.log(single.correct_answer));
  const action_response = useActionData() as {
    correct_answer_num: number;
    individual_correct: boolean[];
  };

  return (
    <>
      <Form method="POST">
        <input
          type="text"
          name="mcqs"
          value={JSON.stringify(data)}
          className="hidden"
          readOnly
        />
        <div className="grid min-h-screen grid-cols-3 gap-4 py-5 max-sm:grid-cols-1 sm:max-lg:grid-cols-2">
          {data.map((mcq, index) => (
            <main
              key={index + mcq.question}
              className="min-h-1/6 font-lightbold text-md flex flex-col items-center justify-between rounded-md bg-indigo-500 p-4 text-black"
            >
              <h1 className="text-2xl font-semibold">{mcq.question}</h1>
              <ul className="w-full space-y-2">
                {mcq.options.map((option, i) => (
                  <div className="flex items-center gap-2" key={i}>
                    <label htmlFor={option} className="flex items-center gap-1">
                      <span>{i + 1}</span>
                      <input
                        type="radio"
                        id={option}
                        name={`mcq${index}`}
                        value={i.toString()}
                        onClick={(e) => console.log(e.currentTarget.value)}
                      />
                      <li key={i}>{option}</li>
                    </label>
                  </div>
                ))}
              </ul>
              <p
                className={twMerge(
                  clsx(
                    "text-md mt-5 w-full self-end rounded-md bg-sky-500 px-2 py-3 font-semibold text-slate-900",
                    action_response?.individual_correct?.[index]
                      ? "bg-fuchsia-400"
                      : null,
                  ),
                )}
              >
                {action_response?.individual_correct?.[index] ? (
                  <span>Correct: {mcq.correct_option}</span>
                ) : (
                  "Tick a correct answer to the above question"
                )}
              </p>
            </main>
          ))}
        </div>
        <div
          className="tooltip tooltip-open tooltip-info sticky right-10 top-10 w-full"
          data-tip="correct"
        >
          {action_response?.correct_answer_num}
        </div>
        <button className="btn btn-success w-full" type="submit">
          Submit
        </button>
      </Form>
    </>
  );
}
